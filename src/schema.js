const { makeSchema } = require('@nexus/schema')
const { nexusPrisma } = require('nexus-plugin-prisma')
const path = require('path')

const {
  DateTime,
  Area, 
  Conta, 
  Atividade, 
  Cargo,
  Cargos_Permissoes,
  SConcentrada,
  Cultura,
  Fase,
  Fertilizante,
  Fertilizantes_Nutrientes,
  Localizacao,
  Log,
  Lote,
  Lotes_Atividades,
  Nutriente,
  Notificacao,
  SNutritiva,
  Permissao,
  Pessoa,
  Reservatorio,
  Solucoes_Contas,
  Solucoes_Fertilizantes_Concentradas,
  Usuario,
  ConectaConta, 
  Setor, 
  Query,
  Mutation
} = require('./schemas')

const schema = makeSchema({
  types: [
    DateTime,
    Area, 
    Conta, 
    Atividade, 
    Cargo,
    Cargos_Permissoes,
    SConcentrada,
    Cultura,
    Fase,
    Fertilizante,
    Fertilizantes_Nutrientes,
    Localizacao,
    Log,
    Lote,
    Lotes_Atividades,
    Nutriente,
    Notificacao,
    SNutritiva,
    Permissao,
    Pessoa,
    Reservatorio,
    Solucoes_Contas,
    Solucoes_Fertilizantes_Concentradas,
    Usuario,
    ConectaConta, 
    Setor, 
    Query,
    Mutation
  ],
  plugins: [nexusPrisma({ experimentalCRUD: true })],
  outputs: {
    schema: path.join(__dirname, 'schema.graphql'),
    typegen: path.join(__dirname, '../prisma/generated', 'nexus.ts')
  }
})

module.exports = schema