const { objectType } = require('@nexus/schema')

const Setor = objectType({
  name: 'Setor',
  definition(t) {
    t.model.id()
    t.model.nome()
    t.model.descricao()
    t.model.created_at()
    t.model.area()
    t.model.reservatorio()
    t.model.lotes()
    t.model.deleted_at()
  }
})

module.exports = Setor