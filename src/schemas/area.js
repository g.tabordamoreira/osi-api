const { objectType } = require('@nexus/schema')

const Area = objectType({
  name: 'Area',
  definition(t) {
    t.model.id()
    t.model.nome()
    t.model.descricao()
    t.model.imagem()
    t.model.tipo()
    t.model.created_at()
    t.model.conta()
    t.model.localizacao()
    t.model.setores()
    t.model.deleted_at()
  }
})

module.exports = Area