const { objectType } = require('@nexus/schema')

const Fase = objectType({
  name: 'Fase',
  definition(t) {
    t.model.id()
    t.model.fase()
    t.model.recomendacao_dias()
    t.model.lotes()
  }
})

module.exports = Fase