const { queryType } = require('@nexus/schema')

const Query = queryType({
  name: 'Query',
  definition(t) {
    t.crud.areas({
      filtering: true,
      ordering: true
    })
    t.crud.area()

    t.crud.atividades({
      filtering: true,
      ordering: true
    })
    t.crud.atividade()

    t.crud.cargos({
      filtering: true,
      ordering: true
    })
    t.crud.cargo()

    t.crud.contas({
      filtering: true,
      ordering: true
    })
    t.crud.conta()

    // ### FAZER MANUALMENTE
    t.crud.concentradas({
      filtering: true,
      ordering: true
    })
    t.crud.concentrada()

    t.crud.culturas({
      filtering: true,
      ordering: true
    })
    t.crud.cultura()

    t.crud.fases({
      filtering: true,
      ordering: true
    })
    t.crud.fase()

    t.crud.fertilizantes({
      filtering: true,
      ordering: true
    })
    t.crud.fertilizante()

    // ### FAZER MANUALMENTE
    // t.crud.fertilizantes_nutrientes({
    //   filtering: true,
    //   ordering: true
    // })

    // ### s no final para indicar MANY
    t.crud.localizacaos({
      filtering: true,
      ordering: true
    })

    t.crud.localizacao()

    t.crud.logs({
      filtering: true,
      ordering: true
    })
    t.crud.log()

    t.crud.lotes({
      filtering: true,
      ordering: true
    })
    t.crud.lote()

    // ### FAZER MANUALMENTE
    t.crud.Lotes_Atividades({
      filtering: true,
      ordering: true
    })
    // t.crud.lotes_atividades()

    t.crud.nutrientes({
      filtering: true,
      ordering: true
    })
    t.crud.nutriente()

    // ### FAZER MANUALMENTE
    t.crud.sNutritivas({
      filtering: true,
      ordering: true
    })
    t.crud.sNutritiva()

    t.crud.notificacaos({
      filtering: true,
      ordering: true
    })
    t.crud.notificacao()

    // ### FAZER MANUALMENTE
    // t.crud.permissoes({
    //   filtering: true,
    //   ordering: true
    // })
    t.crud.permissao()

    t.crud.pessoas({
      filtering: true,
      ordering: true
    })
    t.crud.pessoa()

    t.crud.reservatorios({
      filtering: true,
      ordering: true
    })
    t.crud.reservatorio()

    // ### s no final para indicar MANY
    t.crud.setors({
      filtering: true,
      ordering: true
    })
    t.crud.setor()

    // ### FAZER MANUALMENTE
    // t.crud.solucoes_contas({
    //   filtering: true,
    //   ordering: true
    // })
    // t.crud.solucao_conta()

    // ### FAZER MANUALMENTE
    // t.crud.solucoes_fertilizantes_concentradas({
    //   filtering: true,
    //   ordering: true
    // })
    // t.crud.solucao_fertilizante_concentrada()

    t.crud.usuarios({
      filtering: true,
      ordering: true
    })
    t.crud.usuario()

    // ### FAZER MANUALMENTE
    // t.crud.usuarios_contas_cargos({
    //   filtering: true,
    //   ordering: true
    // })
    // t.crud.usuario_conta_cargo()




    // t.list.field('areas', {
    //   type: 'Area',
    //   resolve: (_, __, { prisma }) => {
    //     return prisma.area.findMany()
    //   }
    // })
    // t.crud.user()
    // t.crud.reviews()
    // t.list.field('postsAprovados', {
    //   type: 'Post',
    //   resolve: (_, __, { prisma }) => {
    //     return prisma.post.findMany({
    //       where: { publicado: true },
    //       orderBy: {
    //         createdAt: 'desc'
    //       }
    //     })
    //   }
    // })
    // t.list.field('buscaAutoresPublicados', {
    //   type: 'User',
    //   args: {
    //     email: nonNull(stringArg())
    //   },
    //   resolve: (_, args, { prisma }) => {
    //     console.log(args)
    //     return prisma.user.findMany({
    //       where: {
    //         email: { contains: args.email },
    //         posts: { some: { publicado: true } } //every
    //       }
    //     })
    //   }
    // })
  }
})

module.exports = Query